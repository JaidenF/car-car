import { useEffect, useState } from "react";
import Modal from "react-bootstrap/Modal";
import ModalHeader from "react-bootstrap/ModalHeader";
import ModalTitle from "react-bootstrap/ModalTitle";
import ModalBody from "react-bootstrap/ModalBody";
import ModalFooter from "react-bootstrap/ModalFooter";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Dropdown from "react-bootstrap/Dropdown";

function SalesRecords() {
  useEffect(() => {
    getData();
  }, []);

  const [salesRecords, setSalesRecords] = useState([]);

  const [sales_person, setSalesRep] = useState("");

  const [salesReps, setSalesReps] = useState([]);

  const [customer, setCustomer] = useState("");

  const [customers, setCustomers] = useState([]);

  const [automobile, setAutomobile] = useState("");

  const [automobiles, setAutomobiles] = useState([]);

  const [price, setPrice] = useState("");

  const [modal, showModal] = useState(false);

  const getData = async () => {
    const response = await fetch("http://localhost:8090/api/sales/");

    if (response.ok) {
      const data = await response.json();
      setSalesRecords(data.sales_records);
    }

    const response2 = await fetch("http://localhost:8090/api/employees/");

    if (response2.ok) {
      const data = await response2.json();
      setSalesReps(data.sales_people);
    }

    const response3 = await fetch("http://localhost:8090/api/customers/");

    if (response3.ok) {
      const data = await response3.json();
      setCustomers(data.potential_customers);
    }

    const response4 = await fetch("http://localhost:8090/api/automobiles/");

    if (response4.ok) {
      const data = await response4.json();
      setAutomobiles(data.automobileVO);
    }
  };

  const handleCreateSalesRep = async () => {
    const data = { price, automobile, sales_person, customer };
    const recordUrl = "http://localhost:8090/api/sales/";
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(recordUrl, fetchConfig);
    if (response.ok) {
      getData();
    }
    closeModal();
  };

  const handleChangePrice = (e) => {
    setPrice(e.target.value);
  };

  const closeModal = () => {
    showModal(false);
  };

  const handleDeleteRecord = async (id) => {
    const data = { id };
    const recordUrl = `http://localhost:8090/api/sales/${id}/`;
    const fetchConfig = {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(recordUrl, fetchConfig);
    if (response.ok) {
      getData();
    }
  };

  return (
    <div className="otherpagescontainer">
      <div className="px-4 py-5 my-5 text-center">
        <h1 className="display-5 fw-bold">Sales Records</h1>
        <Dropdown>
          <Dropdown.Toggle variant="dark">
            {sales_person === "" ? "Sales Rep" : sales_person}
          </Dropdown.Toggle>
          <Dropdown.Menu>
            {salesReps.map((salesRep) => {
              return (
                <Dropdown.Item onClick={() => setSalesRep(salesRep.name)}>
                  {salesRep.name}
                </Dropdown.Item>
              );
            })}
          </Dropdown.Menu>
        </Dropdown>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Sales Person</th>
              <th>Customer</th>
              <th>VIN</th>
              <th>Price</th>
              <th>
                <i
                  onClick={() => {
                    showModal(true);
                  }}
                  className="fas fa-plus-square"
                ></i>
              </th>
            </tr>
          </thead>
          <tbody>
            {salesRecords.map((salesRecord) => {
              return (
                (salesRecord.sales_person.name === sales_person ||
                  sales_person === "") && (
                  <tr key={salesRecord.id}>
                    <td>{salesRecord.sales_person.name}</td>
                    <td>{salesRecord.customer.name}</td>
                    <td>{salesRecord.automobile.vin}</td>
                    <td>{`$${salesRecord.price}`}</td>
                    <td>
                      <i
                        onClick={() => {
                          handleDeleteRecord(salesRecord.id);
                        }}
                        className="fa fa-trash"
                        aria-hidden="true"
                      ></i>
                    </td>
                  </tr>
                )
              );
            })}
          </tbody>
        </table>
        <Modal show={modal} onHide={closeModal}>
          <ModalHeader closeButton>
            <ModalTitle>Add Sales Record</ModalTitle>
          </ModalHeader>
          <ModalBody>
            <Form>
              <Dropdown className="dropdown-spacing">
                <Dropdown.Toggle variant="dark">
                  {sales_person === "" ? "Sales Rep" : sales_person}
                </Dropdown.Toggle>
                <Dropdown.Menu>
                  {salesReps.map((salesRep) => {
                    return (
                      <Dropdown.Item onClick={() => setSalesRep(salesRep.id)}>
                        {salesRep.name}
                      </Dropdown.Item>
                    );
                  })}
                </Dropdown.Menu>
              </Dropdown>
              <Dropdown className="dropdown-spacing">
                <Dropdown.Toggle variant="dark">
                  {customer === "" ? "Customer Name" : customer}
                </Dropdown.Toggle>
                <Dropdown.Menu>
                  {customers.map((customer) => {
                    return (
                      <Dropdown.Item onClick={() => setCustomer(customer.id)}>
                        {customer.name}
                      </Dropdown.Item>
                    );
                  })}
                </Dropdown.Menu>
              </Dropdown>
              <Dropdown className="dropdown-spacing">
                <Dropdown.Toggle variant="dark">
                  {automobile === "" ? "VIN" : automobile}
                </Dropdown.Toggle>
                <Dropdown.Menu>
                  {automobiles.map((automobile) => {
                    return (
                      <Dropdown.Item
                        onClick={() => setAutomobile(automobile.vin)}
                      >
                        {automobile.vin}
                      </Dropdown.Item>
                    );
                  })}
                </Dropdown.Menu>
              </Dropdown>
              <Form.Group className="mb-3">
                <Form.Control
                  value={price}
                  onChange={(e) => {
                    handleChangePrice(e);
                  }}
                  type="text"
                  placeholder="Price"
                />
              </Form.Group>
            </Form>
          </ModalBody>
          <ModalFooter>
            <Button onClick={handleCreateSalesRep} variant="success">
              Create
            </Button>
          </ModalFooter>
        </Modal>
      </div>
    </div>
  );
}

export default SalesRecords;
