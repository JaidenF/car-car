import { useEffect, useState } from "react";
import Modal from "react-bootstrap/Modal";
import ModalHeader from "react-bootstrap/ModalHeader";
import ModalTitle from "react-bootstrap/ModalTitle";
import ModalBody from "react-bootstrap/ModalBody";
import ModalFooter from "react-bootstrap/ModalFooter";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";
import Dropdown from "react-bootstrap/Dropdown";

function Models() {
  useEffect(() => {
    getData();
  }, []);

  const [models, setModels] = useState([]);

  const [manufacturers, setManufacturers] = useState([]);

  const [manufacturer_id, setManufacturer] = useState("");

  const [picture_url, setPictureUrl] = useState("");

  const [name, setManufacturerName] = useState("");

  const [modal, showModal] = useState(false);

  const getData = async () => {
    const response = await fetch("http://localhost:8100/api/models/");

    if (response.ok) {
      const data = await response.json();
      setModels(data.models);
      console.log(data.models);
    }

    const response2 = await fetch("http://localhost:8100/api/manufacturers/");

    if (response2.ok) {
      const data = await response2.json();
      setManufacturers(data.manufacturers);
      console.log(data.manufacturers);
    }
  };

  const handleCreateManufacturer = async () => {
    const data = { name, picture_url, manufacturer_id };
    const modelUrl = "http://localhost:8100/api/models/";
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(modelUrl, fetchConfig);
    if (response.ok) {
      getData();
    }
    closeModal();
  };

  const handleChangeManufacturerName = (e) => {
    setManufacturerName(e.target.value);
  };

  const handleChangePictureUrl = (e) => {
    setPictureUrl(e.target.value);
  };

  const closeModal = () => {
    showModal(false);
  };

  const handleDeleteModel = async (id) => {
    const data = { id };
    const modelUrl = `http://localhost:8100/api/models/${id}/`;
    const fetchConfig = {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(modelUrl, fetchConfig);
    if (response.ok) {
      getData();
    }
  };

  return (
    <div className="otherpagescontainer">
      <div className="px-4 py-5 my-5 text-center">
        <h1 className="display-5 fw-bold">Models</h1>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Name</th>
              <th>Manufacturer</th>
              <th>Picture</th>
              <th>
                <i
                  onClick={() => {
                    showModal(true);
                  }}
                  className="fas fa-plus-square"
                ></i>
              </th>
            </tr>
          </thead>
          <tbody>
            {models.map((model) => {
              return (
                <tr key={model.href}>
                  <td>{model.name}</td>
                  <td>{model.manufacturer.name}</td>
                  <td>
                    <img src={model.picture_url} className="small-image"></img>
                  </td>
                  <td>
                    <i
                      onClick={() => {
                        handleDeleteModel(model.id);
                      }}
                      className="fa fa-trash"
                      aria-hidden="true"
                    ></i>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
        <Modal show={modal} onHide={closeModal}>
          <ModalHeader closeButton>
            <ModalTitle>Add Model</ModalTitle>
          </ModalHeader>
          <ModalBody>
            <Form>
              <Form.Group className="mb-3">
                <Form.Control
                  value={name}
                  onChange={(e) => {
                    handleChangeManufacturerName(e);
                  }}
                  type="text"
                  placeholder="Model"
                />
              </Form.Group>
              <Form.Group className="mb-3">
                <Form.Control
                  value={picture_url}
                  onChange={(e) => {
                    handleChangePictureUrl(e);
                  }}
                  type="text"
                  placeholder="Picture Url"
                />
              </Form.Group>
              <Dropdown>
                <Dropdown.Toggle variant="dark">
                  {manufacturer_id === "" ? "Manufacturer" : manufacturer_id}
                </Dropdown.Toggle>
                <Dropdown.Menu>
                  {manufacturers.map((manufacturer) => {
                    return (
                      <Dropdown.Item
                        onClick={() => setManufacturer(manufacturer.id)}
                      >
                        {manufacturer.name}
                      </Dropdown.Item>
                    );
                  })}
                </Dropdown.Menu>
              </Dropdown>
            </Form>
          </ModalBody>
          <ModalFooter>
            <Button onClick={handleCreateManufacturer} variant="success">
              Create
            </Button>
          </ModalFooter>
        </Modal>
      </div>
    </div>
  );
}

export default Models;
