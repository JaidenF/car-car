import { useEffect, useState } from "react";
import Modal from "react-bootstrap/Modal";
import ModalHeader from "react-bootstrap/ModalHeader";
import ModalTitle from "react-bootstrap/ModalTitle";
import ModalBody from "react-bootstrap/ModalBody";
import ModalFooter from "react-bootstrap/ModalFooter";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";

function Manufacturers() {
  useEffect(() => {
    getData();
  }, []);

  const [manufacturers, setManufacturers] = useState([]);

  const [name, setManufacturerName] = useState("");

  const [modal, showModal] = useState(false);

  const getData = async () => {
    const response = await fetch("http://localhost:8100/api/manufacturers/");

    if (response.ok) {
      const data = await response.json();
      setManufacturers(data.manufacturers);
    }
  };

  const handleCreateManufacturer = async () => {
    const data = { name };
    const manufacturerNameUrl = "http://localhost:8100/api/manufacturers/";
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(manufacturerNameUrl, fetchConfig);
    if (response.ok) {
      getData();
    }
    closeModal();
  };

  const handleChangeManufacturerName = (e) => {
    setManufacturerName(e.target.value);
  };

  const closeModal = () => {
    showModal(false);
  };

  const handleDeleteManufacturer = async (id) => {
    const data = { id };
    const manufacturerNameUrl = `http://localhost:8100/api/manufacturers/${id}/`;
    const fetchConfig = {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(manufacturerNameUrl, fetchConfig);
    if (response.ok) {
      getData();
    }
  };

  return (
    <div className="otherpagescontainer">
      <div className="px-4 py-5 my-5 text-center">
        <h1 className="display-5 fw-bold">Manufacturers</h1>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Name</th>
              <th>
                <i
                  onClick={() => {
                    showModal(true);
                  }}
                  className="fas fa-plus-square"
                ></i>
              </th>
            </tr>
          </thead>
          <tbody>
            {manufacturers.map((manufacturer) => {
              return (
                <tr key={manufacturer.href}>
                  <td>{manufacturer.name}</td>
                  <td>
                    <i
                      onClick={() => {
                        handleDeleteManufacturer(manufacturer.id);
                      }}
                      className="fa fa-trash"
                      aria-hidden="true"
                    ></i>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
        <Modal show={modal} onHide={closeModal}>
          <ModalHeader closeButton>
            <ModalTitle>Add Manufacturer</ModalTitle>
          </ModalHeader>
          <ModalBody>
            <Form>
              <Form.Group className="mb-3">
                <Form.Control
                  value={name}
                  onChange={(e) => {
                    handleChangeManufacturerName(e);
                  }}
                  type="text"
                  placeholder="Manufacturer"
                />
              </Form.Group>
            </Form>
          </ModalBody>
          <ModalFooter>
            <Button onClick={handleCreateManufacturer} variant="success">
              Create
            </Button>
          </ModalFooter>
        </Modal>
      </div>
    </div>
  );
}

export default Manufacturers;
