import { useEffect, useState } from "react";
import Modal from "react-bootstrap/Modal";
import ModalHeader from "react-bootstrap/ModalHeader";
import ModalTitle from "react-bootstrap/ModalTitle";
import ModalBody from "react-bootstrap/ModalBody";
import ModalFooter from "react-bootstrap/ModalFooter";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";

function Automobiles() {
  useEffect(() => {
    getData();
  }, []);

  const [automobiles, setAutomobiles] = useState([]);

  const [vin, setVin] = useState("");

  const [color, setColor] = useState("");

  const [year, setYear] = useState("");

  const [modal, showModal] = useState(false);

  const getData = async () => {
    const response = await fetch("http://localhost:8100/api/automobiles/");

    if (response.ok) {
      const data = await response.json();
      setAutomobiles(data.autos);
    }
  };

  const handleCreateManufacturer = async () => {
    const data = { vin, color, year };
    const manufacturerNameUrl = "http://localhost:8100/api/manufacturers/";
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(manufacturerNameUrl, fetchConfig);
    if (response.ok) {
      getData();
    }
    closeModal();
  };

  const handleChangeVin = (e) => {
    setVin(e.target.value);
  };

  const handleChangeColor = (e) => {
    setColor(e.target.value);
  };

  const handleChangeYear = (e) => {
    setYear(e.target.value);
  };

  const closeModal = () => {
    showModal(false);
  };

  const handleDeleteAuto = async (vin) => {
    const data = { vin };
    const AutoNameUrl = `http://localhost:8100/api/automobiles/${vin}/`;
    const fetchConfig = {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(AutoNameUrl, fetchConfig);
    if (response.ok) {
      getData();
    }
  };

  return (
    <div className="otherpagescontainer">
      <div className="px-4 py-5 my-5 text-center">
        <h1 className="display-5 fw-bold">Automobiles</h1>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>VIN</th>
              <th>Color</th>
              <th>Year</th>
              <th>Model</th>
              <th>Manufacturer</th>
              <th>
                <i
                  onClick={() => {
                    showModal(true);
                  }}
                  className="fas fa-plus-square"
                ></i>
              </th>
            </tr>
          </thead>
          <tbody>
            {automobiles.map((automobile) => {
              return (
                <tr key={automobile.href}>
                  <td>{automobile.vin}</td>
                  <td>{automobile.color}</td>
                  <td>{automobile.year}</td>
                  <td>{automobile.model.name}</td>
                  <td>{automobile.model.manufacturer.name}</td>
                  <td>
                    <i
                      onClick={() => {
                        handleDeleteAuto(automobile.vin);
                      }}
                      className="fa fa-trash"
                      aria-hidden="true"
                    ></i>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
        <Modal show={modal} onHide={closeModal}>
          <ModalHeader closeButton>
            <ModalTitle>Add Automobile</ModalTitle>
          </ModalHeader>
          <ModalBody>
            <Form>
              <Form.Group className="mb-3">
                <Form.Control
                  value={vin}
                  onChange={(e) => {
                    handleChangeVin(e);
                  }}
                  type="text"
                  placeholder="VIN"
                />
              </Form.Group>
            </Form>
            <Form>
              <Form.Group className="mb-3">
                <Form.Control
                  value={color}
                  onChange={(e) => {
                    handleChangeColor(e);
                  }}
                  type="text"
                  placeholder="Color"
                />
              </Form.Group>
            </Form>
            <Form>
              <Form.Group className="mb-3">
                <Form.Control
                  value={year}
                  onChange={(e) => {
                    handleChangeYear(e);
                  }}
                  type="text"
                  placeholder="Year"
                />
              </Form.Group>
            </Form>
          </ModalBody>
          <ModalFooter>
            <Button onClick={handleCreateManufacturer} variant="success">
              Create
            </Button>
          </ModalFooter>
        </Modal>
      </div>
    </div>
  );
}

export default Automobiles;
