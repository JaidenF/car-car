import { useEffect, useState } from "react";
import Modal from "react-bootstrap/Modal";
import ModalHeader from "react-bootstrap/ModalHeader";
import ModalTitle from "react-bootstrap/ModalTitle";
import ModalBody from "react-bootstrap/ModalBody";
import ModalFooter from "react-bootstrap/ModalFooter";
import Button from "react-bootstrap/Button";
import Form from "react-bootstrap/Form";

function Reps() {
  useEffect(() => {
    getData();
  }, []);

  const [salesPersons, setSalesPersons] = useState([]);

  const [name, setManufacturerName] = useState("");

  const [employee_id, setEmployeeId] = useState();

  const [modal, showModal] = useState(false);

  const getData = async () => {
    const response = await fetch("http://localhost:8090/api/employees/");

    if (response.ok) {
      const data = await response.json();
      setSalesPersons(data.sales_people);
    }
  };

  const handleCreateSalesRep = async () => {
    const data = { name, employee_id };
    const manufacturerNameUrl = "http://localhost:8090/api/employees/";
    const fetchConfig = {
      method: "POST",
      body: JSON.stringify(data),
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(manufacturerNameUrl, fetchConfig);
    if (response.ok) {
      getData();
    }
    closeModal();
  };

  const handleChangeManufacturerName = (e) => {
    setManufacturerName(e.target.value);
  };

  const handleChangeEmployeeId = (e) => {
    setEmployeeId(e.target.value);
  };

  const closeModal = () => {
    showModal(false);
  };

  const handleDeleteManufacturer = async (id) => {
    const data = { id };
    const manufacturerNameUrl = `http://localhost:8090/api/employees/${id}/`;
    const fetchConfig = {
      method: "DELETE",
      headers: {
        "Content-Type": "application/json",
      },
    };
    const response = await fetch(manufacturerNameUrl, fetchConfig);
    if (response.ok) {
      getData();
    }
  };

  return (
    <div className="px-4 py-5 my-5 text-center">
      <h1 className="display-5 fw-bold">Sales Team</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Name</th>
            <th>Employee Id</th>
            <th>
              <i
                onClick={() => {
                  showModal(true);
                }}
                className="fas fa-plus-square"
              ></i>
            </th>
          </tr>
        </thead>
        <tbody>
          {salesPersons.map((salesPerson) => {
            return (
              <tr key={salesPerson.employee_id}>
                <td>{salesPerson.name}</td>
                <td>{salesPerson.employee_id}</td>
                <td>
                  <i
                    onClick={() => {
                      handleDeleteManufacturer(salesPerson.id);
                    }}
                    className="fa fa-trash"
                    aria-hidden="true"
                  ></i>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
      <Modal show={modal} onHide={closeModal}>
        <ModalHeader closeButton>
          <ModalTitle>Add Sales Rep</ModalTitle>
        </ModalHeader>
        <ModalBody>
          <Form>
            <Form.Group className="mb-3">
              <Form.Control
                value={name}
                onChange={(e) => {
                  handleChangeManufacturerName(e);
                }}
                type="text"
                placeholder="Name"
              />
            </Form.Group>
            <Form.Group className="mb-3">
              <Form.Control
                value={employee_id}
                onChange={(e) => {
                  handleChangeEmployeeId(e);
                }}
                type="text"
                placeholder="Id"
              />
            </Form.Group>
          </Form>
        </ModalBody>
        <ModalFooter>
          <Button onClick={handleCreateSalesRep} variant="success">
            Create
          </Button>
        </ModalFooter>
      </Modal>
    </div>
  );
}

export default Reps;

