# Generated by Django 4.0.3 on 2023-01-24 17:40

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('service_rest', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='serviceappointment',
            name='automobile',
            field=models.ForeignKey(default=None, on_delete=django.db.models.deletion.DO_NOTHING, related_name='automobile', to='service_rest.automobilevo'),
        ),
    ]
