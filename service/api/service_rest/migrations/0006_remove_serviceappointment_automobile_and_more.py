# Generated by Django 4.0.3 on 2023-01-24 18:07

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('service_rest', '0005_remove_serviceappointment_active_and_more'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='serviceappointment',
            name='automobile',
        ),
        migrations.RemoveField(
            model_name='serviceappointment',
            name='status',
        ),
        migrations.AddField(
            model_name='serviceappointment',
            name='active',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='serviceappointment',
            name='date',
            field=models.DateTimeField(),
        ),
        migrations.AlterField(
            model_name='serviceappointment',
            name='owner_name',
            field=models.CharField(max_length=200),
        ),
        migrations.AlterField(
            model_name='serviceappointment',
            name='technician',
            field=models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, related_name='technician', to='service_rest.technician'),
        ),
        migrations.AlterField(
            model_name='serviceappointment',
            name='vin',
            field=models.CharField(max_length=20),
        ),
    ]
