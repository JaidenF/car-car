# CarCar

Team:

* Person 1 - Which microservice?
* Person 2 - Which microservice?

## Design

## Service microservice

Explain your models and integration with the inventory
microservice, here.

## Sales microservice

Explain your models and integration with the inventory
microservice, here.

I created a sales microservice to use vehicles from the inventory for current sales and sales records. With my backend views I can get/post/delete vehicles that are currently in inventory and I can also update them in insomnia or my database. The front can create and display employees/customers and sales records.