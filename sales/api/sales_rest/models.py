from django.db import models


class SalesPerson(models.Model):
    name = models.CharField(max_length=150)
    employee_id = models.PositiveSmallIntegerField(unique=True)

    def __str__(self):
        return self.name


class PotentialCustomer(models.Model):
    name = models.CharField(max_length=150)
    address = models.CharField(max_length=250)
    phone_number = models.CharField(max_length=50)

    def __str__(self):
        return self.name


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)
    import_href = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return self.vin


class SalesRecord(models.Model):
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name="automobile",
        on_delete=models.CASCADE,

    )
    sales_person = models.ForeignKey(
        SalesPerson,
        related_name="sales_person",
        on_delete=models.CASCADE,
    )
    customer = models.ForeignKey(
        PotentialCustomer,
        related_name="potential_customer",
        on_delete=models.CASCADE,
    )
    price = models.PositiveIntegerField()

